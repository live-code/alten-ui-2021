import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule) },
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule) },
  { path: 'weather', loadChildren: () => import('./features/weather/weather.module').then(m => m.WeatherModule) },
  { path: 'uikit', loadChildren: () => import('./features/uikit/uikit.module').then(m => m.UikitModule) },
  { path: 'uikit-directives', loadChildren: () => import('./features/uikit-directives/uikit-directives.module').then(m => m.UikitDirectivesModule) },
  { path: 'translate-demo', loadChildren: () => import('./features/translate-demo/translate-demo.module').then(m => m.TranslateDemoModule) },
  { path: '', pathMatch: 'full', redirectTo: 'home'},
  { path: 'demo-pipes', loadChildren: () => import('./features/demo-pipes/demo-pipes.module').then(m => m.DemoPipesModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
