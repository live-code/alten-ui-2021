import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from './components/card/card.module';
import { GridModule } from './components/grid/grid.module';
import { WeatherModule } from './components/weather/weather.module';
import { AccordionModule } from './components/accordion/accordion.module';
import { StaticMapComponent } from './components/static-map/static-map.component';
import { LeafletComponent } from './components/leaflet/leaflet.component';
import { LeafletOldComponent } from './components/leaflet/leaflet-old.component';
import { PadDirective } from './directives/pad.directive';
import { HighlightDirective } from './directives/highlight.directive';
import { UrlDirective } from './directives/url.directive';
import { IfLoggedDirective } from './directives/if-logged.directive';
import { LoaderDirective } from './directives/loader.directive';
import { FormatGbPipe } from './pipes/format-gb.pipe';
import { UserIdPipe } from './pipes/user-id.pipe';
import { TranslatePipe } from './pipes/translate.pipe';

@NgModule({
  declarations: [
    StaticMapComponent,
    LeafletComponent,
    LeafletOldComponent,
    PadDirective,
    HighlightDirective,
    UrlDirective,
    IfLoggedDirective,
    LoaderDirective,
    FormatGbPipe,
    UserIdPipe,
    TranslatePipe,
  ],
  imports: [
    CommonModule,
    CardModule,
    GridModule,
    WeatherModule,
    AccordionModule,

  ],
  exports: [
    CardModule,
    GridModule,
    WeatherModule,
    AccordionModule,
    StaticMapComponent,
    LeafletComponent,
    LeafletOldComponent,
    PadDirective,
    HighlightDirective,
    UrlDirective,
    IfLoggedDirective,
    LoaderDirective,
    FormatGbPipe,
    UserIdPipe,
    TranslatePipe,
  ]
})
export class SharedModule { }
