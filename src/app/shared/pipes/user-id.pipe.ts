import { Pipe, PipeTransform } from '@angular/core';
import { Observable, pipe } from 'rxjs';
import { User } from '../../model/user';
import { HttpClient } from '@angular/common/http';

@Pipe({
  name: 'userId',
})
export class UserIdPipe implements PipeTransform {
  constructor(private http: HttpClient) {
  }

  transform(id: number): Observable<User> {
    return this.http.get<User>(`https://jsonplaceholder.typicode.com/users/${id}`)
  }

}
