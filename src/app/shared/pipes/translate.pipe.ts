import { Pipe, PipeTransform } from '@angular/core';
import { TranslationService } from '../../core/translation.service';

@Pipe({
  name: 'translate',
})
export class TranslatePipe implements PipeTransform {

  constructor(private translationService: TranslationService) {
    console.log('translate')
  }

  transform(key: string, lang: string): string {
    return this.translationService.getValue(key)
  }

}
