import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatGb',
  pure: false

})
export class FormatGbPipe implements PipeTransform {

  transform(value: number, format: 'mb' | 'byte' = 'mb', is1024: boolean = false): number {
    switch (format) {
      case 'mb': return value * 1000;
      case 'byte': return value * 1000000;
    }

  }

}
