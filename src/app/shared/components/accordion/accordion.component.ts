import {
  Component,
  ContentChildren, Input,
  QueryList,
} from '@angular/core';
import { GroupComponent } from './group.component';

@Component({
  selector: 'at-accordion',
  template: `
    <div style="border: 3px solid black; padding: 10px">
      <ng-content></ng-content>
    </div>
  `,
})
export class AccordionComponent {
  @ContentChildren(GroupComponent) groups!: QueryList<GroupComponent>;
  previousGroupsQueryList!: QueryList<GroupComponent>
  previousGroup!: GroupComponent;
  @Input() openMultiple = false;


  ngDoCheck() {
    if (this.previousGroupsQueryList !== this.groups) {
      this.groups.toArray()[0].opened = true;
      this.previousGroup = this.groups.toArray()[0];
      this.previousGroupsQueryList = this.groups;
    }
  }


  ngAfterContentInit() {
   this.groups.toArray()[0].opened = true;
    for (let g of this.groups) {
      g.onToggle.subscribe(() => {
        if (this.previousGroup && !this.openMultiple) {
          this.previousGroup.opened = false;
        }
        g.opened = true;
        this.previousGroup = g;
      })
    }
  }
}
