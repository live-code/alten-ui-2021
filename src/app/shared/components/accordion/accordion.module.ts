import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccordionComponent } from './accordion.component';
import { GroupComponent } from './group.component';



@NgModule({
  declarations: [
    AccordionComponent,
    GroupComponent
  ],
  exports: [
    AccordionComponent,
    GroupComponent
  ],
  imports: [
    CommonModule
  ]
})
export class AccordionModule { }
