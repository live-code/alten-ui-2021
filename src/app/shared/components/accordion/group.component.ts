import { Component, Input, EventEmitter, Output } from '@angular/core';
import { AccordionComponent } from './accordion.component';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'at-group',
  animations: [
    trigger('collapsable', [
      state('opened', style({ height: '*' })),
      state('closed', style({ height: 0, padding: 0 })),
      transition('opened <=> closed', [
        animate('1s cubic-bezier(0.83, 0, 0.17, 1)')
      ])
    ])
  ],
  template: `
    <div class="mypanel">
      <div class="title" (click)="onToggle.emit()">
        {{title}}
      </div>
      
      <div class="body" [@collapsable]="opened ? 'opened' : 'closed'">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [`
    .mypanel {
      background-color: white;
      color: #222;
    }

    .mypanel .title {
      background-color: #e2e2e2;
      width: 100%;
      padding: 10px;
      cursor: pointer;
      border-bottom: 1px solid #999;
    }

    .mypanel .body {
      padding: 20px;
      /*height: 200px;*/
      overflow: hidden;
      /*transition: 1s all ease-in-out;*/
    }
/*    .mypanel .closed {
      height: 0;
      padding: 0;
      
    }*/
  `]
})
export class GroupComponent {
  @Input() title: string = '';
  @Input() opened: boolean = false;
  @Output() onToggle = new EventEmitter();

}
