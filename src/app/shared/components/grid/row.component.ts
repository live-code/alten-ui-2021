import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'at-row',
  template: `
    <div class="row">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
  ]
})
export class RowComponent {
  @Input() mq: 'xs' | 'md' | 'xl' | 'sm' = 'sm'
}

