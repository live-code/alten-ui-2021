import { Component, HostBinding, Input, OnInit, Optional } from '@angular/core';
import { RowComponent } from './row.component';

@Component({
  selector: 'at-col',
  template: `
    <ng-content></ng-content>      
  `,
  styles: [
  ]
})
export class ColComponent {
  @Input() cols: string | number = 12;

  @HostBinding() get class() {
    return 'col-' + (this.parentRow?.mq || 'sm') + '-' + this.cols;
  }

  constructor(@Optional() private parentRow: RowComponent) {
    console.log(this.parentRow?.mq)
  }

}
