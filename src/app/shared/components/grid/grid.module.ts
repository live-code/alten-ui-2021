import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RowComponent } from './row.component';
import { ColComponent } from './col.component';



@NgModule({
  declarations: [
    RowComponent,
    ColComponent,
  ],
  exports: [
    RowComponent,
    ColComponent,
  ],
  imports: [
    CommonModule
  ]
})
export class GridModule { }
