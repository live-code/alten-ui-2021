import { ChangeDetectionStrategy, Component, Input } from '@angular/core';


@Component({
  selector: 'at-static-map',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <img 
      [attr.width]="width"
      *ngIf="locations"
      [src]="getImage()" 
    >
  `,
})
export class StaticMapComponent {
  API = 'Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn';
  @Input() width: number | string = '100%';
  @Input() locations: string[] = [];
  @Input() zoom: number = 10;

  getImage() {
    return `https://www.mapquestapi.com/staticmap/v5/map?key=${this.API}&locations=${this.locations.join('||')}&size=600,400`
  }
}
