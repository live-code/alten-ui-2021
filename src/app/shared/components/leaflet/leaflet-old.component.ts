import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import * as L from 'leaflet';

var greenIcon = L.icon({
  iconUrl: './assets/marker.png',
  iconSize:     [40, 40], // size of the icon
  // shadowSize:   [50, 64], // size of the shadow
  iconAnchor:   [20, 20], // point of the icon which will correspond to marker's location
  // shadowAnchor: [4, 62],  // the same for the shadow
  popupAnchor:  [0, -20] // point from which the popup should open relative to the iconAnchor
});

@Component({
  selector: 'at-leaflet-old',
  template: `
    <div #host style="width: 100%; height: 300px" >
      mappa
    </div>
    
  `,
  styles: [
  ]
})
export class LeafletOldComponent {
  @ViewChild('host', { static: false }) host!: ElementRef<HTMLDivElement>

  @Input() set coords(val: [number, number]) {
    if (this.host) {
      if (!this.map) {
        this.init(val);
      } else {
        this.marker
          .setLatLng(val)
          .bindPopup(val.join(','))
        this.map.setView(val)
      }
    } else {
      setTimeout(() => {
        this.init(val);
      })
    }
  }

  @Input() set zoom(val: number) {
    if (this.map) {
      this.map.setZoom(val || 5)
    }
  }

  map!: L.Map;
  marker!: L.Marker;

  init(val: [number, number]): void {
    this.map = L.map(this.host.nativeElement).setView(val, 13);
    L.tileLayer('https://tiles.stadiamaps.com/tiles/alidade_smooth_dark/{z}/{x}/{y}{r}.png', {
      maxZoom: 20,
    }).addTo(this.map);

    this.marker = L.marker(val, {icon: greenIcon}).addTo(this.map)
      .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
      .openPopup();
  }

}
