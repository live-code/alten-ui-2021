import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import * as L from 'leaflet';

var greenIcon = L.icon({
  iconUrl: './assets/marker.png',
  iconSize:     [40, 40], // size of the icon
  // shadowSize:   [50, 64], // size of the shadow
  iconAnchor:   [20, 20], // point of the icon which will correspond to marker's location
  // shadowAnchor: [4, 62],  // the same for the shadow
  popupAnchor:  [0, -20] // point from which the popup should open relative to the iconAnchor
});

@Component({
  selector: 'at-leaflet',
  template: `
    <div #host style="width: 100%; height: 300px" >
      mappa
    </div>
  `,
  styles: [
  ]
})
export class LeafletComponent implements OnChanges {
  @ViewChild('host', { static: true }) host!: ElementRef<HTMLDivElement>
  @Input() coords!: [number, number];

  @Input() zoom: number = 5;
  map!: L.Map;
  marker!: L.Marker;

  init(): void {
    this.map = L.map(this.host.nativeElement).setView(this.coords, 13);
    L.tileLayer('https://tiles.stadiamaps.com/tiles/alidade_smooth_dark/{z}/{x}/{y}{r}.png', {
      maxZoom: 20,
    }).addTo(this.map);

    this.marker = L.marker(this.coords, {icon: greenIcon}).addTo(this.map)
      .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
      .openPopup();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.coords?.firstChange) {
      this.init();
    }
    if (changes.coords) {
      this.marker
        .setLatLng(changes.coords.currentValue)
        .bindPopup(changes.coords.currentValue.join(','))
      this.map.setView(changes.coords.currentValue)
    }

    if (changes.zoom) {
      this.map.setZoom(changes.zoom.currentValue)
    }
  }

}
