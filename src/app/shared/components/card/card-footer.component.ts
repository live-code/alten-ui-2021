import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'at-card-footer',
  template: `
    <div>
      <ng-content></ng-content>...
    </div>
  `,
  styles: [
  ]
})
export class CardFooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
