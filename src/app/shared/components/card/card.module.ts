import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card.component';
import { CardBodyComponent } from './card-body.component';
import { CardFooterComponent } from './card-footer.component';



@NgModule({
  declarations: [
    CardComponent, CardBodyComponent, CardFooterComponent
  ],
  exports: [
    CardComponent, CardBodyComponent, CardFooterComponent
  ],
  imports: [
    CommonModule
  ]
})
export class CardModule { }
