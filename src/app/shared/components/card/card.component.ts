import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'at-card',
  template: `
    <div class="card mb-2">
      <div 
        class="card-header bg-dark text-white" 
        (click)="opened = !opened"
      > 
        {{title}}
        <div class="pull-right">
          <i [ngClass]="icon" *ngIf="icon" (click)="iconClickHandler($event)"></i>
        </div>
      </div>
      
      <div class="card-body" [ngClass]="{hide: !opened}">
        <ng-content select="at-card-body"></ng-content>
      </div>
      
      <div class="card-footer">
        <ng-content select="at-card-footer"></ng-content>
      </div>
      
    </div>
  `,
  styles: [`
    .hide { display: none}
  `]
})
export class CardComponent {
  @Input() title: string | null = null;
  @Input() icon: string = '';
  @Output() iconClick = new EventEmitter()

  opened = true;

  iconClickHandler(e: MouseEvent) {
    e.stopPropagation();
    this.iconClick.emit();
  }
}
