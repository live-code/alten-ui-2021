import { Component, ElementRef, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'at-weather',
  template: `
      <h1>{{city}}</h1>
      <h2 *ngIf="meteo">{{meteo.main.temp}}</h2>
  `,
})
export class WeatherComponent implements OnChanges {
  @Input() city: string | null = null;
  meteo: any;

  constructor(private http: HttpClient) {}

  ngOnChanges(): void {
    this.http.get(`http://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
      .subscribe(res => {
        this.meteo = res;
      })
  }

}
