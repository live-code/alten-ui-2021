import { Directive, HostBinding, Input, OnDestroy, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService, Role } from '../../core/auth.service';
import { Subject, Subscription } from 'rxjs';
import { distinctUntilChanged, filter, first, takeUntil, tap, withLatestFrom } from 'rxjs/operators';

@Directive({
  selector: '[atIfLogged]'
})
export class IfLoggedDirective implements OnDestroy {
  @Input() atIfLogged!: Role
  destroy$ = new Subject();

  constructor(
    private template: TemplateRef<any>,
    private view: ViewContainerRef,
    private authService: AuthService
  ) {
    this.authService.isLogged()
      .pipe(
        takeUntil(this.destroy$),
        distinctUntilChanged(),
        tap(() => view.clear()),
        withLatestFrom(this.authService.auth),
        filter(([isLogged, auth]) => {
          return isLogged && this.atIfLogged === auth?.role
        })
      )
      .subscribe(() => {
        view.createEmbeddedView(template)
      })
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
