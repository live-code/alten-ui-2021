import { ComponentFactoryResolver, EventEmitter, Directive, Input, Output, Type, ViewContainerRef } from '@angular/core';
import { CardComponent } from '../components/card/card.component';
import { StaticMapComponent } from '../components/static-map/static-map.component';

export const COMPONENTS: {[key: string]: Type<any> } = {
  card: CardComponent,
  staticmap: StaticMapComponent
}

@Directive({
  selector: '[atLoader]'
})
export class LoaderDirective {
  @Input() atLoader!: {
    type: string,
    events?: string[],
    props: any;
  };

  @Output() action = new EventEmitter<{  eventType: string, value: any }>()

  constructor(
    private view: ViewContainerRef,
    private resolver: ComponentFactoryResolver
  ) {

  }

  ngOnInit() {
    const factory = this.resolver.resolveComponentFactory(COMPONENTS[this.atLoader.type])
    const ref = this.view.createComponent(factory);

    for (let key in this.atLoader.props) {
      ref.instance[key] = this.atLoader.props[key];
    }

    if (this.atLoader.events) {
      for (let key of this.atLoader.events) {
        console.log(ref.instance[key], key)
        ref.instance[key].subscribe((val: any) => {
          this.action.emit({ eventType: key, value: val})
        })
      }
    }

  }


}
