import { Directive, ElementRef, HostBinding, Input, Renderer2, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[atHighlight]'
})
export class HighlightDirective {
  @Input() set atHighlight(val: string | undefined | null) {
    this.render.setStyle(this.el.nativeElement, 'background-color', val || 'yellow')
  }

  @Input() border: boolean = false;
  @Input() type: 'solid' | 'dashed' | 'dotted' = 'solid'
  @Input() color: string = 'black';

 /* @HostBinding('style.border') get getBorder() {
    if (this.border) {
      return `1px ${this.type} ${this.color}`
    }
    return 'none'
  }*/

  constructor(private el: ElementRef, private render: Renderer2) {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.render.setStyle(
      this.el.nativeElement,
      'border',
      `1px ${this.type} ${this.color}`
    )
  }
}





