import { Directive, ElementRef, HostBinding, Input, Renderer2, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';

@Directive({
  selector: '[atPad]'
})
export class PadDirective {
  @Input() atPad: 1 | 2 | 3 = 1;
  @Input() color!: string;

  constructor(private el: ElementRef, private renderer: Renderer2) {

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.atPad) {
      this.renderer.setStyle(this.el.nativeElement, 'padding', (changes.atPad.currentValue * 10) + 'px')
    }

    if (changes.color) {
      this.renderer.setStyle(this.el.nativeElement, 'color', changes.color.currentValue)
    }
  }
}
