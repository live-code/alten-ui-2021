import { Directive, ElementRef, HostBinding, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[atUrl]'
})
export class UrlDirective {
  @Input() atUrl!: string;

  @HostBinding('style.cursor') cursor = 'pointer'

  @HostListener('click')
  clickHandler() {
    window.open(this.atUrl)
  }

  @HostListener('mouseover', ['$event.target'])
  overHandler(target: HTMLElement) {
    this.render.setStyle(target, 'color', 'red')
  }

  @HostListener('mouseleave', ['$event'])
  leaveHandler(e: MouseEvent) {
    this.render.setStyle(e.target, 'color', 'black')
  }

  constructor(private render: Renderer2) {
  }
}
