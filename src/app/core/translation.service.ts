import { Injectable } from '@angular/core';
import { TRANSLATTIONS_CONTENT } from './translation';

export type Language = 'en' | 'it';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {
  language: Language = 'en';
  translations!: { [key: string]: any }

  setLanguage(lang: Language) {
    this.language = lang;
    this.load(lang)
  }

  private load(lang: Language) {
    this.translations = TRANSLATTIONS_CONTENT[lang];
  }

  getLanguage(): Language {
    return this.language;
  }

  getValue(key: string) {
    return this.translations[key];
  }
}
