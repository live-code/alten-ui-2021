import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export type Role = 'admin' | 'guest';
export interface Auth {
  displayName: string;
  role: Role
}
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth = new BehaviorSubject<Auth | null>(null);

  constructor() { }

  login() {
    this.auth.next({
      displayName: 'mario rossi',
      role: 'guest'
    })
  }

  logout() {
    this.auth.next(null)
  }

  isLogged(): Observable<boolean> {
    return this.auth
      .pipe(
        map(value => !!value)
      )
  }
}
