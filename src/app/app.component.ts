import { Component } from '@angular/core';

@Component({
  selector: 'at-root',
  template: `
    <button routerLink="home">home</button>
    <button routerLink="login">login</button>
    <button routerLink="weather">weather</button>
    <button routerLink="uikit">uikit</button>
    <button routerLink="uikit-directives">uikit-directives</button>
    <button routerLink="translate-demo">translate-demo</button>
    <button routerLink="demo-pipes">pipes</button>
    <button *atIfLogged="'guest'" routerLink="catalog">catalog</button>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {

  constructor() {
  }
}
