export interface Member {
  id: number;
  age: number;
  gender: 'M' | 'F';
  name: string;
}
