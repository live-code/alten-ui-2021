import { Component, OnInit } from '@angular/core';
import { User } from '../../model/user';
import { Member } from '../../model/member';

@Component({
  selector: 'at-demo-pipes',
  template: `
    <input type="text" placeholder="Search name" [(ngModel)]="filterSearchText">
    <input type="number" placeholder="Search name" [(ngModel)]="filterAge">

    <select [(ngModel)]="filterGender">
      <option value="">All</option>
      <option value="F">Female</option>
      <option value="M">Male</option>
    </select>

    <select [(ngModel)]="orderBy">
      <option value="ASC">asc</option>
      <option value="DESC">desc</option>
    </select>

    {{filterGender}} {{orderBy}} {{filterSearchText}}
    <hr>
    <li *ngFor="let m of (members 
        | filterMembers: filterGender : filterAge : orderBy | async)
        | filterByText: filterSearchText
    ">
      {{m.name}} - {{m.age}} - {{m.gender}}
    </li>
  `,
  styles: [
  ]
})
export class DemoPipesComponent {
  members: Member[] = [
    { id: 1, name: 'Michele', age: 30, gender: 'M' },
    { id: 2, name: 'Fabio', age: 25, gender: 'M' },
    { id: 3, name: 'Silvia', age: 27, gender: 'F' }
  ];

  filterSearchText = '';
  filterAge = 0;
  filterGender: 'M' | 'F' | '' = ''
  orderBy: 'ASC' | 'DESC' = 'ASC'

  constructor() { }
}
