import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoPipesRoutingModule } from './demo-pipes-routing.module';
import { DemoPipesComponent } from './demo-pipes.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { FilterMembersPipe } from './pipes/filter-by-gender.pipe';
import { SortUsersPipe } from './pipes/sort-users.pipe';
import { FilterByTextPipe } from './pipes/filter-by-text.pipe';


@NgModule({
  declarations: [
    DemoPipesComponent,
    FilterMembersPipe,
    SortUsersPipe,
    FilterByTextPipe
  ],
  imports: [
    CommonModule,
    DemoPipesRoutingModule,
    FormsModule,
    SharedModule
  ]
})
export class DemoPipesModule { }
