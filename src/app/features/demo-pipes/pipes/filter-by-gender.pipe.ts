import { Pipe, PipeTransform } from '@angular/core';
import { Member } from '../../../model/member';
import { from, Observable, of } from 'rxjs';
import { filter, map, tap, toArray } from 'rxjs/operators';

@Pipe({
  name: 'filterMembers'
})
export class FilterMembersPipe implements PipeTransform {

  transform(
    items: Member[],
    gender: 'M' | 'F' | '',
    age: number,
    sortBy: 'ASC' | 'DESC' = 'ASC'
  ):Observable<Member[]> {

   /*
   if (gender === '') {
      return items;
    }
    return items.filter(item => item.gender === gender);
    if (gender === '') {
      return of(items);
    }
    */

    return from(items)
      .pipe(
        tap(item => console.log(item)),
        filter(item => {
          return gender === '' || item.gender === gender;
        }),
        filter(item => item.age > age),
        toArray(),
        map(items => {
          const result = items.sort((a, b) => a.name.localeCompare(b.name));
          return sortBy === 'ASC' ? result : result.reverse();
        })
      )
  }

}
