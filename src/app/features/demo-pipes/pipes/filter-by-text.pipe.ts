import { Pipe, PipeTransform } from '@angular/core';
import { Member } from '../../../model/member';

@Pipe({
  name: 'filterByText'
})
export class FilterByTextPipe implements PipeTransform {

  transform(items: Member[] | null, text: string): Member[] {
    if (text === '') {
      return items || [];
    }
    return items?.filter(item => {
      return item.name.toLowerCase().includes(text.toLowerCase());
    }) || [];
  }

}
