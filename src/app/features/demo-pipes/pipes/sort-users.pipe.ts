import { Pipe, PipeTransform } from '@angular/core';
import { Member } from '../../../model/member';

@Pipe({
  name: 'sortUsers'
})
export class SortUsersPipe implements PipeTransform {

  transform(items: Member[], order: 'ASC' | 'DESC'): unknown {
    return null;
  }

}
