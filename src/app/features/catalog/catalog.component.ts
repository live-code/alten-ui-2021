import { Component } from '@angular/core';
import { UserService } from './services/user.service';

@Component({
  selector: 'at-catalog',
  template: `
    <at-card title="User Form">
      <at-catalog-form (save)="userService.save($event)"></at-catalog-form>
    </at-card>
    

    <at-card title="user list">
      <at-catalog-list
        [users]="userService.users"
        (deleteUser)="userService.deleteUserHandler($event)"
      ></at-catalog-list>
    </at-card>
    
    <pre>{{userService.users | json}}</pre>
  `,
})
export class CatalogComponent {
  constructor(public userService: UserService) {}
}

