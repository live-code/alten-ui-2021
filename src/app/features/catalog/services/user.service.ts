import { Injectable } from '@angular/core';
import { User } from '../../../model/user';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class UserService {
  users: User[] | null = null;

  constructor(private http: HttpClient) {
    http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => {
        this.users = res
      })
  }

  save(user: User) {
    if (this.users) {
      this.users = [...this.users, user]
    }
  }

  deleteUserHandler(id: number) {
    this.http.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
      .subscribe(() => {
        if (this.users) {
          this.users = this.users.filter(u => u.id !== id);
        }
      })
  }
}
