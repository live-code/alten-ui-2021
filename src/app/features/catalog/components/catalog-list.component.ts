import { ChangeDetectionStrategy, Component, EventEmitter, Output, Input } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'at-catalog-list',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    
    <div class="bg"> List</div>
    
    <at-catalog-list-item 
      *ngFor="let user of users"
      [user]="user"
      (deleteUser)="deleteUser.emit($event)"
    >
    </at-catalog-list-item>
    
  `,
})
export class CatalogListComponent {
  @Input() users: User[] | null = null;
  @Output() deleteUser = new EventEmitter<number>()
}

