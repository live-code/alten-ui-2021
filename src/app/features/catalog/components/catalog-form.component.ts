import { EventEmitter, ChangeDetectionStrategy, Component, Output, ChangeDetectorRef, OnInit } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'at-catalog-form',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <form #f="ngForm" (submit)="save.emit(f.value)">
      <input type="text" #nameRef="ngModel" ngModel name="name" required minlength>
      <input type="text" ngModel name="surname">
      <button [disabled]="f.invalid">SAVE</button>
    </form>
  `,
})
export class CatalogFormComponent {
  @Output() save = new EventEmitter<User>();

  constructor(private cd: ChangeDetectorRef) {
    setTimeout(() => {
      this.cd.detectChanges();
    })
  }
}
