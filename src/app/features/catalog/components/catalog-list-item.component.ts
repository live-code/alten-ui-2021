import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'at-catalog-list-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  // encapsulation: ViewEncapsulation.ShadowDom,
  template: `
    <li class="bg">
      {{user.name}}
      <i class="fa fa-arrow-circle-down" (click)="open = !open"></i>
      <i class="fa fa-trash" (click)="deleteUser.emit(user.id)"></i>
      <div *ngIf="open">
        {{user.address.city}} <br>
        {{user.email}}
      </div>
    </li>
  `,
  styles: [`
   
  `]
})
export class CatalogListItemComponent {
  @Input() user!: User;
  @Output() deleteUser = new EventEmitter<number>()
  open = false;
}
