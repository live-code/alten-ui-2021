import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { FormsModule } from '@angular/forms';
import { CatalogListComponent } from './components/catalog-list.component';
import { CatalogFormComponent } from './components/catalog-form.component';
import { CatalogListItemComponent } from './components/catalog-list-item.component';
import { CardComponent } from '../../shared/components/card/card.component';
import { SharedModule } from '../../shared/shared.module';
import { CardModule } from '../../shared/components/card/card.module';


@NgModule({
  declarations: [
    CatalogComponent,
    CatalogListComponent,
    CatalogFormComponent,
    CatalogListItemComponent,
  ],
  imports: [
    CommonModule,
    CatalogRoutingModule,
    FormsModule,
    CardModule
  ]
})
export class CatalogModule { }
