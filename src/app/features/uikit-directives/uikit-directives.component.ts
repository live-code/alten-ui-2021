import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserIdPipe } from '../../shared/pipes/user-id.pipe';
import { FormatGbPipe } from '../../shared/pipes/format-gb.pipe';

@Component({
  selector: 'at-uikit-directives',
  template: `
    
    <select [(ngModel)]="format">
      <option>mb</option>
      <option>byte</option>
    </select>
    
    <div>{{8 | formatGb: format : true}}</div>
    <div>{{8 | formatGb: 'mb' : true}}</div>
    <div>{{8 | formatGb: 'byte'}}</div>
    <div>{{8 | formatGb}}</div>
    
    <div>{{2 | userId | async | json}}</div>
    <div *ngIf="(4 | userId | async) as user">
      {{user.address.city}}
      {{user.name}}
    </div>
    
    <input type="text" ngModel>
    
    <div [atPad]="val" [color]="(color | async) || 'red'">xxx</div>
    <div [atPad]="3">xxx</div>
    
    <div>ciao a <span atHighlight="cyan" [border]="true" type="dashed" color="red">tutti</span></div>
    <div>ciao a <span atHighlight="pink">tutti</span></div>
    <div>ciao a <span atHighlight>tutti</span></div>
    
    <ng-container *ngIf="true" >
      <div *ngFor="let user of []"></div>
    </ng-container>
    <hr>
    
    
    <div routerLink="..." routerLinkActive="bg"></div>
    <button atUrl="http://www.google.com">visit</button>
    
    visit my <span atHighlight atUrl="http://www.google.com">website</span>
  `,
})
export class UikitDirectivesComponent implements OnInit {
  val: 1 | 2 | 3 = 3;
  color = new BehaviorSubject<string>('pink')
  format: 'mb' | 'byte' = 'mb';

  constructor() {
    setTimeout(() => {
      this.val = 1;
      this.color.next('blue')
    }, 2000)
  }

  ngOnInit(): void {
    console.log(new FormatGbPipe().transform(9))
  }

}
