import {
  AfterViewInit,
  Component,
  ElementRef,
  OnChanges,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { GroupComponent } from '../../shared/components/accordion/group.component';

@Component({
  selector: 'at-weather-page',
  template: `
    <at-weather [city]="value"></at-weather>
    <button 
      (click)="value = 'Palermo'">Palermo</button>

    <button
      (click)="value = 'Milan'">Milan</button>
    
    <br>
    <br>
    <at-accordion [openMultiple]="false">
      <at-group *ngFor="let item of ['Trieste', 'Palermo', 'Roma']" [title]="item">
        <at-static-map [locations]="[item]" width="100%"></at-static-map>
      </at-group>
      <at-group title="group 1">
        <input type="text">
        <input type="text">
        <input type="text">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut, recusandae, temporibus? Asperiores blanditiis exercitationem iusto laudantium molestiae molestias non, quo tenetur voluptates. Culpa cum modi ratione rem! Corporis neque, reprehenderit.
      </at-group>
      <at-group title="group 2">
        <button>123</button>
      </at-group>
      <at-group title="Mappa">
        <at-static-map [locations]="vaules" [width]="width"></at-static-map>
      </at-group>
    </at-accordion>
    <div #host>abc</div>

 
    <at-static-map [locations]="vaules" [width]="100"></at-static-map>
  `,
  styles: [
  ]
})
export class WeatherComponent {
  // @ViewChild(GroupComponent, { read: ElementRef }) group!: ElementRef<HTMLElement>;
  @ViewChild(GroupComponent) group!: ElementRef<HTMLElement>;
  @ViewChild('host', { static: true }) host!: ElementRef<HTMLDivElement>
  value: string | null = null;

  vaules = ['Roma', 'Palermo', 'Milan'];
  width: number | string = '100%';

  constructor() {
    this.value = 'Rome';
  }


  ngOnInit(): void {
    setTimeout(() => {
      this.vaules = ['London', 'Berlin']
      this.width = 100;
    }, 1000)
  }
}
