import { Component, ComponentFactoryResolver, OnInit, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../core/auth.service';
import { CardComponent } from '../../shared/components/card/card.component';

@Component({
  selector: 'at-login',
  template: `

    <input type="text">
    <input type="text">
    <button (click)="authService.login()">sign in</button>
    <button (click)="authService.logout()">Logout</button>
    
    <div *atIfLogged="'admin'">
      area protetta
    </div>

    <div *atIfLogged="'guest'">
      area per i guest
    </div>
    
    <div [atLoader]="{ type: 'card', props: {title: 'ciao'}}"></div>
    <div
      [atLoader]="{ type: 'card', events: ['iconClick'], props: {icon: 'fa fa-link', title: 'miao'}}"
      (action)="doSomething($event)"
    ></div>
    <div [atLoader]="{ type: 'staticmap', props: { zoom: 10, locations: ['palermo', 'rome']}}"></div>
  `,
  styles: [
  ]
})
export class LoginComponent {

  constructor(
    public authService: AuthService,

  ) {

  }

  doSomething($event: { eventType: string; value: any }) {
    console.log($event)
  }
}
