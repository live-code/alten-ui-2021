import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'at-home',
  template: `
    
    
    <at-card title="top" icon="fa fa-link" (iconClick)="openUrl('http://www.pluto.com')">
      <at-card-body>
        <input type="text">
        <input type="text">
        <input type="text">
      </at-card-body>
      <at-card-footer>bla bla </at-card-footer>
    </at-card>
    
    <div *ngIf="visible">
      un altro pannello
    </div>
    
    <at-card title="rows example">
      <at-card-body>
        <at-row mq="sm">
          <at-col [cols]="6">left</at-col>
          <at-col cols="6">right</at-col>
        </at-row>
      </at-card-body>
    </at-card>
    
    
    
  `,
  styles: [
  ]
})
export class HomeComponent {
  visible = false;

  openUrl(url: string): void {
    window.open(url);
  }

}
