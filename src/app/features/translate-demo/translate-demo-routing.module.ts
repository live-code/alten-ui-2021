import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TranslateDemoComponent } from './translate-demo.component';

const routes: Routes = [{ path: '', component: TranslateDemoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TranslateDemoRoutingModule { }
