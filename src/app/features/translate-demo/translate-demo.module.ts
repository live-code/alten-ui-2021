import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateDemoRoutingModule } from './translate-demo-routing.module';
import { TranslateDemoComponent } from './translate-demo.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    TranslateDemoComponent
  ],
  imports: [
    CommonModule,
    TranslateDemoRoutingModule,
    FormsModule,
    SharedModule
  ]
})
export class TranslateDemoModule { }
