import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../core/translation.service';

@Component({
  selector: 'at-translate-demo',
  template: `
    {{translatationService.getLanguage()}}
    
    {{translatationService.getValue('HELLO')}}
    {{ 'HELLO' | translate: translatationService.language }}
    
    <hr>
    <button (click)="translatationService.setLanguage('it')">it</button>
    <button (click)="translatationService.setLanguage('en')">en</button>
  `,
})
export class TranslateDemoComponent {

  constructor(public translatationService: TranslationService) {
    translatationService.setLanguage('it')
  }

}
