import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'at-uikit',
  template: `
    <button (click)="zoom = zoom - 1">-</button>
    <button (click)="zoom = zoom + 1">+</button>
    <button (click)="coords = [55, 14]">location 1</button>
    <button (click)="coords = [33, 24]">location 2</button>
    <at-leaflet [zoom]="zoom" [coords]="coords"></at-leaflet>
    <at-leaflet-old [zoom]="zoom" [coords]="coords"></at-leaflet-old>
  `,
})
export class UikitComponent implements OnInit {
  zoom = 5;
  coords: [number, number] = [43, 13]; // Tuple

  ngOnInit(): void {
  }

}
